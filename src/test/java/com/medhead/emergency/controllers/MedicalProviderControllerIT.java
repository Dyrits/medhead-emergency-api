package com.medhead.emergency.controllers;

import com.medhead.emergency.ContainerizedDatabase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class MedicalProviderControllerIT extends ContainerizedDatabase {

    @Autowired
    public MockMvc mock;

    @Tag("Integration")
    @DisplayName("The request should return all the medical providers")
    @Test
    public void findAll() throws Exception {
        mock.perform(get("/medical-providers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(7)))
                .andExpect(jsonPath("$[0].name", is("St. Thomas' Hospital")))
                .andExpect(jsonPath("$[1].address", is("Hensol Castle Park, Hensol Rd, Pontyclun CF72 8JX, United Kingdom")))
                .andExpect(jsonPath("$[2].availability", is(10)));
    }


    @Tag("Integration")
    @DisplayName("The request should not find any medical provider with 50 beds available")
    @Test
    public void searchWithoutSpecialityWith50Beds() throws Exception {
        mock.perform(get("/medical-providers/search")
                .param("amount", "50")
                .param("latitude", "51.5073")
                .param("longitude", "-0.1657"))
                .andExpect(status().isNotFound());
    }

    @Tag("Integration")
    @DisplayName("The request should return the closest medical provider without speciality but with 10 beds available")
    @Test
    public void searchWithoutSpecialityWith10Beds() throws Exception {
        mock.perform(get("/medical-providers/search")
                        .param("amount", "10")
                        .param("latitude", "51.5073")
                        .param("longitude", "-0.1657"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("Thurloe Street Dental and Implant Centre")))
                .andExpect(jsonPath("availability", is(10)));
    }

    @Tag("Integration")
    @DisplayName("The request should return the closest medical provider without the speciality but enough beds available")
    @Test
    public void searchWithSpecialityWith25Beds() throws Exception {
        mock.perform(get("/medical-providers/search")
                        .param("speciality", "Anaesthetics")
                        .param("amount", "25")
                        .param("latitude", "51.5073")
                        .param("longitude", "-0.1657"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("St. George's Hospital")))
                .andExpect(jsonPath("availability", is(0)));
    }

    @Tag("Integration")
    @DisplayName("The request should return the closest medical provider without the provided speciality (because unavailable)")
    @Test
    public void searchWithSpecialityWith5Beds() throws Exception {
        mock.perform(get("/medical-providers/search")
                        .param("speciality", "Nuclear medicine")
                        .param("amount", "5")
                        .param("latitude", "51.5072")
                        .param("longitude", "-0.1276"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("St. Thomas' Hospital")))
                .andExpect(jsonPath("availability", is(15)));
    }

    @Tag("Integration")
    @DisplayName("The request should return the closest medical provider amon three found, filtered using the MapBox API")
    @Test
    public void searchWithSpecialityWithMapBoxAPI() throws Exception {
        mock.perform(get("/medical-providers/search")
                        .param("latitude", "51.649113")
                        .param("longitude", "-0.204372"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("St. Thomas' Hospital")))
                .andExpect(jsonPath("availability", is(19)));
    }
}
