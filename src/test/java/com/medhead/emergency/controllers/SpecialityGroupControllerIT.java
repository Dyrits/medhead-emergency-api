package com.medhead.emergency.controllers;

import com.medhead.emergency.ContainerizedDatabase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SpecialityGroupControllerIT extends ContainerizedDatabase {

    @Autowired
    public MockMvc mock;

    @Tag("Integration")
    @DisplayName("Getting all the speciality groups")
    @Test
    public void findAll() throws Exception {
        mock.perform(get("/groups"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].name", is("Anaesthetics")))
                .andExpect(jsonPath("$[11].name", is("Surgical")));
    }

}
