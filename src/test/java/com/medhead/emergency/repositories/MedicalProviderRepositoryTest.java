package com.medhead.emergency.repositories;

import com.medhead.emergency.ContainerizedDatabase;
import com.medhead.emergency.entities.MedicalProvider;
import com.medhead.emergency.entities.Speciality;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.collections4.IterableUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MedicalProviderRepositoryTest extends ContainerizedDatabase {

    @Autowired
    private MedicalProviderRepository repository;

    private List<MedicalProvider> providers;

    @BeforeEach
    void initialize() {
        this.providers = new ArrayList<>();
        String[] names = {"First Hospital", "Second Hospital", "Third Hospital"};
        String[] addresses = {"First address", "Second address", "Third address"};
        double[] latitudes = {5.0, 10.0, 15.0};
        double[] longitudes = {-5.0, -10.0, -15};
        int[] availabilities = {5, 10, 15};
        for (int index = 0; index < 3; index ++) {
            MedicalProvider provider = new MedicalProvider(names[index], addresses[index], latitudes[index], longitudes[index], availabilities[index]);
            provider.addSpeciality(new Speciality("New Speciality"));
            this.providers.add(provider);
        }
    }

    @AfterEach
    void nullify () { this.providers = null; }

    @Tag("Repository")
    @DisplayName("The correct number of medical providers are found (according to the speciality, availability and location area)")
    @Test
    void findTwoByWithSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by speciality, availability and location area:
        List<MedicalProvider> providers = this.findAllByWithSpeciality();
        // Then only two medical providers are found:
        assertThat(providers.size()).isEqualTo(2);
    }

    @Tag("Repository")
    @DisplayName("The medical providers found (according to the speciality, availability and location area) are instance of MedicalProvider")
    @Test
    void findInstancesOfMedicalProviderByWithSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by speciality, availability and location area:
        List<MedicalProvider> providers = this.findAllByWithSpeciality();
        // Then the medical providers are instance of MedicalProvider:
        assertThat(providers.get(0)).isExactlyInstanceOf(MedicalProvider.class);
        assertThat(providers.get(1)).isExactlyInstanceOf(MedicalProvider.class);
    }


    @Tag("Repository")
    @DisplayName("The medical providers found (according to the speciality, availability and location area) have the correct properties")
    @Test
    void findPropertiesByWithSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by speciality, availability and location area:
        List<MedicalProvider> providers = this.findAllByWithSpeciality();
        // Then the medical providers have the correct properties of the first and second medical providers:
        assertThat(providers.get(0).getName()).isEqualTo("First Hospital");
        assertThat(providers.get(1).getName()).isEqualTo("Second Hospital");
        assertThat(providers.get(0).getAddress()).isEqualTo("First address");
        assertThat(providers.get(1).getAddress()).isEqualTo("Second address");
    }
    private List<MedicalProvider> findAllByWithSpeciality() {
        return IterableUtils.toList(repository.findAllBySpecialitiesNameAndAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                "New Speciality",
                5,
                0.0,
                10.0,
                -10.0,
                0.0
        ));
    }

    @Tag("Repository")
    @DisplayName("The correct number of medical providers are found (according to the availability and location area)")
    @Test
    void findOneByWithoutSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by availability and location area:
        List<MedicalProvider> providers = this.findAllByWithoutSpeciality();
        // Then only one medical provider is found:
        assertThat(providers.size()).isEqualTo(1);
    }

    @Tag("Repository")
    @DisplayName("The medical providers found (according to the availability and location area) are instance of MedicalProvider")
    @Test
    void findInstancesOfMedicalProviderByWithoutSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by availability and location area:
        List<MedicalProvider> providers = this.findAllByWithoutSpeciality();
        // Then the medical providers is an instance of MedicalProvider:
        assertThat(providers.get(0)).isExactlyInstanceOf(MedicalProvider.class);
    }

    @Tag("Repository")
    @DisplayName("The medical provider found (according to the availability and location area) have the correct properties")
    @Test
    void findPropertiesByWithoutSpeciality() {
        // Given the following medical providers:
        repository.saveAll(this.providers);
        // When the medical providers are found by availability and location area:
        List<MedicalProvider> providers = this.findAllByWithoutSpeciality();
        // Then the medical provider have the correct properties of the third medical provider:
        assertThat(providers.get(0).getName()).isEqualTo("Third Hospital");
        assertThat(providers.get(0).getAddress()).isEqualTo("Third address");
    }

    private List<MedicalProvider> findAllByWithoutSpeciality() {
        return IterableUtils.toList(repository.findAllByAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                10,
                15.0,
                20.0,
                -20.0,
                -15.0
        ));
    }
}
