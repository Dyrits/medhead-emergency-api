package com.medhead.emergency.services;

import com.medhead.emergency.ContainerizedDatabase;
import com.medhead.emergency.repositories.MedicalProviderRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MedicalProviderServiceTest extends ContainerizedDatabase {

    @Mock
    private MedicalProviderRepository repository;
    private MedicalProviderService service;
    private ArgumentCaptor<String> specialityCaptor;
    private ArgumentCaptor<Integer> availabilityCaptor;
    private ArgumentCaptor<Double> minLatitudeCaptor;
    private ArgumentCaptor<Double> maxLatitudeCaptor;
    private ArgumentCaptor<Double> minLongitudeCaptor;
    private ArgumentCaptor<Double> maxLongitudeCaptor;


    @BeforeEach
    void initialize() {
        this.service = new MedicalProviderService(repository);
        this.specialityCaptor = ArgumentCaptor.forClass(String.class);
        this.availabilityCaptor = ArgumentCaptor.forClass(Integer.class);
        this.minLatitudeCaptor = ArgumentCaptor.forClass(Double.class);
        this.maxLatitudeCaptor = ArgumentCaptor.forClass(Double.class);
        this.minLongitudeCaptor = ArgumentCaptor.forClass(Double.class);
        this.maxLongitudeCaptor = ArgumentCaptor.forClass(Double.class);
    }

    @AfterEach
    void nullify() {
        this.service = null;
        this.specialityCaptor = null;
        this.availabilityCaptor = null;
        this.minLatitudeCaptor = null;
        this.maxLatitudeCaptor = null;
        this.minLongitudeCaptor = null;
        this.maxLongitudeCaptor = null;
    }

    @Tag("Service")
    @DisplayName("The findAllBy method (with speciality) should use the correct repository method to search for medical providers")
    @Test
    void findAllByWithSpeciality() {
        // Given the following coordinates:
        Map<String, Double> coordinates = Map.of(
                "minLatitude", 10.0,
                "maxLatitude", 20.0,
                "minLongitude", 15.0,
                "maxLongitude", 25.0
        );
        // When calling the method with the service:
        this.service.findAllBy("New Speciality", 5, coordinates);
        // Then the (separately tested) repository should be called with the same parameters:
        verify(this.repository).findAllBySpecialitiesNameAndAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                "New Speciality",
                5,
                10.0,
                20.0,
                15.0,
                25.0
        );
    }

    @Tag("Service")
    @DisplayName("The repository should be called with the correct parameters when the findAllBy (with speciality) method is called")
    @Test
    void findAllByParametersWithSpeciality() {
        // Given the following coordinates:
        Map<String, Double> coordinates = Map.of(
                "minLatitude", 10.0,
                "maxLatitude", 25.0,
                "minLongitude", -5.0,
                "maxLongitude", 25.0
        );
        // When calling the method with the service and the repository:
        this.service.findAllBy("New Speciality", 15, coordinates);
        verify(this.repository).findAllBySpecialitiesNameAndAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                this.specialityCaptor.capture(),
                this.availabilityCaptor.capture(),
                this.minLatitudeCaptor.capture(),
                this.maxLatitudeCaptor.capture(),
                this.minLongitudeCaptor.capture(),
                this.maxLongitudeCaptor.capture()
        );
        // Then the parameters captured during the method call should be identical:
        assertThat(this.specialityCaptor.getValue()).isEqualTo("New Speciality");
        assertThat(this.availabilityCaptor.getValue()).isEqualTo(15);
        assertThat(this.minLatitudeCaptor.getValue()).isEqualTo(10.0);
        assertThat(this.maxLatitudeCaptor.getValue()).isEqualTo(25.0);
        assertThat(this.minLongitudeCaptor.getValue()).isEqualTo(-5.0);
        assertThat(this.maxLongitudeCaptor.getValue()).isEqualTo(25.0);
    }

    @Tag("Service")
    @DisplayName("The findAllBy method (without speciality) should use the correct repository method to search for medical providers")
    @Test
    void FindAllByWithoutSpeciality() {
        // Given the following coordinates:
        Map<String, Double> coordinates = Map.of(
                "minLatitude", 5.0,
                "maxLatitude", 25.0,
                "minLongitude", 15.0,
                "maxLongitude", 20.0
        );
        // When calling the method with the service:
        this.service.findAllBy( 10, coordinates);
        // Then the (separately tested) repository should be called with the same parameters:
        verify(this.repository).findAllByAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                10,
                5.0,
                25.0,
                15.0,
                20.0
        );
    }

    @Tag("Service")
    @DisplayName("The repository should be called with the correct parameters when the findAllBy (without speciality) method is called")
    @Test
    void findAllByParametersWithoutSpeciality() {
        // Given the following coordinates:
        Map<String, Double> coordinates = Map.of(
                "minLatitude", 0.0,
                "maxLatitude", 10.0,
                "minLongitude", 5.0,
                "maxLongitude", 15.0
        );
        // When calling the method with the service and the repository:
        this.service.findAllBy( 10, coordinates);
        verify(this.repository).findAllByAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                this.availabilityCaptor.capture(),
                this.minLatitudeCaptor.capture(),
                this.maxLatitudeCaptor.capture(),
                this.minLongitudeCaptor.capture(),
                this.maxLongitudeCaptor.capture()
        );
        // Then the parameters captured during the method call should be identical:
        assertThat(this.availabilityCaptor.getValue()).isEqualTo(10);
        assertThat(this.minLatitudeCaptor.getValue()).isEqualTo(0.0);
        assertThat(this.maxLatitudeCaptor.getValue()).isEqualTo(10.0);
        assertThat(this.minLongitudeCaptor.getValue()).isEqualTo(5.0);
        assertThat(this.maxLongitudeCaptor.getValue()).isEqualTo(15.0);
    }
}