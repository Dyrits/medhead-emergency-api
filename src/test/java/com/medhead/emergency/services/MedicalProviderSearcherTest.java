package com.medhead.emergency.services;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MedicalProviderSearcherTest {

    private MedicalProviderSearcher searcher;

    @BeforeEach
    void initialize() { searcher = new MedicalProviderSearcher(); }

    @AfterEach
    void nullify() { searcher = null; }

    @Tag("Conversion")
    @DisplayName("Conversion from miles to kilometers")
    @ParameterizedTest(name = "{0} miles equals {1} kilometers")
    @CsvSource({ "0.0, 0.0", "1.0, 1.60934", "0.6213727366498067, 1.0" })
    void convertMilesToKilometers(double miles, double kilometers) {
        // Given a distance in miles.
        // When the distance is converted to kilometers.
        double conversion = searcher.convertMilesToKilometers(miles);
        // Then the conversion is correct.
        assertThat(conversion).isEqualTo(kilometers);
    }

    @Tag("Conversion")
    @DisplayName("Conversion from miles to degrees of latitude")
    @ParameterizedTest(name = "{0} miles equals {1} degrees of latitude")
    @CsvSource({ "0.0, 0.0", "1.0, 0.0144927536231884", "69.00000000000003, 1" })
    void convertMilesToLatitude(double miles, double latitude) {
        // Given a distance in miles.
        // When the distance is converted to degrees of latitude.
        double conversion = searcher.convertMilesToLatitude(miles);
        // Then the conversion is correct.
        assertThat(conversion).isEqualTo(latitude);
    }

}
