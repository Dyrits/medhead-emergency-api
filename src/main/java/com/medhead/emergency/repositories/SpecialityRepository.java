package com.medhead.emergency.repositories;

import com.medhead.emergency.entities.Speciality;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialityRepository extends CrudRepository<Speciality, Long> {
    Iterable<Speciality> findALlByGroupName(String group);
}
