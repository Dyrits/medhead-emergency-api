package com.medhead.emergency.repositories;

import com.medhead.emergency.entities.MedicalProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalProviderRepository extends CrudRepository<MedicalProvider, Long> {
    Iterable<MedicalProvider> findAllBySpecialitiesNameAndAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
            String speciality,
            int availability,
            double minLatitude,
            double maxLatitude,
            double minLongitude,
            double maxLongitude
    );

    Iterable<MedicalProvider> findAllByAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
            int availability,
            Double minLatitude,
            Double maxLatitude,
            Double minLongitude,
            Double maxLongitude
    );
}
