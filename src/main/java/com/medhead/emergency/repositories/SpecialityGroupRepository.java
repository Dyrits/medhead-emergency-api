package com.medhead.emergency.repositories;

import com.medhead.emergency.entities.SpecialityGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialityGroupRepository extends CrudRepository<SpecialityGroup, Long> {
}
