package com.medhead.emergency.services;

import com.medhead.emergency.entities.MedicalProvider;
import com.medhead.emergency.services.utilities.MapboxAPIMatrix;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.ProtocolException;
import java.util.HashMap;
import java.util.Map;

@Service
@Getter
@Slf4j
public class MedicalProviderSearcher {
    private MedicalProviderService service;
    private GeodeticCalculator calculator;
    private Ellipsoid reference;
    GlobalCoordinates departure;

    @Autowired
    public void setService(MedicalProviderService service) {
        this.service = service;
    }

    /**
     * Search for a medical provider with the given speciality and amount of beds around given coordinates.
     * @param speciality: The speciality required from the medical provider.
     * @param amount: The amount of beds required from the medical provider.
     * @param latitude: The latitude of the departure location.
     * @param longitude: The longitude of the departure location.
     * @return A medical provider.
     * @throws ProtocolException: If the Mapbox Matrix API call fails.
     */
    public MedicalProvider search(String speciality, int amount, double latitude, double longitude) throws ProtocolException {
        initGeodesy(latitude, longitude);
        Iterable<MedicalProvider> providers = searchAllBy(speciality, amount);
        if (IterableUtils.size(providers) == 1) { return IterableUtils.get(providers, 0); }
        if (IterableUtils.size(providers) == 0) {
            log.info("No provider found with speciality: " + speciality + " and " + amount + " beds.");
            providers = searchAllBy(amount);
            if (IterableUtils.size(providers) == 1) { return IterableUtils.get(providers, 0); }
        }
        if (IterableUtils.size(providers) > 1) {
            log.info("Multiple providers found. Looking up for the closest one.");
            return MapboxAPIMatrix.search(this.departure, providers);
        }
        return null;
    }
    public double convertMilesToKilometers(double miles){ return miles * 1.60934; }
    public double convertMilesToLatitude(double miles){ return miles * 0.0144927536231884;}

    /**
     * Search for medical providers with the given speciality and amount of beds around the departure location.
     * The radius of the search starts at 1.5 miles and is doubled every time a provider is not found, up to 48 miles.
     * @param speciality: The speciality required from the medical provider.
     * @param amount: The amount of beds required from the medical provider.
     * @return An Iterable of medical providers.
     */
    private Iterable<MedicalProvider> searchAllBy(String speciality, int amount) {
        Iterable<MedicalProvider> providers = null;
        double radius = 1.5;
        while (radius<= 48 && IterableUtils.size(providers) == 0) {
            log.info(speciality == null ?
                    "Searching for a provider without speciality with " + amount + " beds in a radius of " + radius + " miles." :
                    "Searching for a provider with speciality: " + speciality + " with " + amount + " beds in a radius of " + radius + " miles."
            );
            Map<String, Double> coordinates = getCoordinates(radius);
            providers = speciality != null ? service.findAllBy(speciality, amount, coordinates) : service.findAllBy(amount, coordinates);
            log.info(IterableUtils.size(providers) + " providers found.");
            radius *= 2;
        }
        return providers;
    }

    /**
     * Search for medical providers with the given amount of beds around the departure location.
     * This method calls the searchAllBy() method with a null speciality.
     * @param amount: The amount of beds required from the medical provider.
     * @return An Iterable of medical providers.
     */
    private Iterable<MedicalProvider> searchAllBy(int amount) { return searchAllBy(null, amount); }

    /**
     * Get a map of coordinates for the search.
     * The coordinates correspond to a square, with the center of the square being the departure location.
     * @param radius: The radius in miles.
     * @return A map of coordinates.
     */
    private Map<String, Double> getCoordinates(double radius) {
        Map<String, Double> coordinates = new HashMap<>();
        double latitude = convertMilesToLatitude(radius);
        coordinates.put("minLatitude", this.departure.getLatitude() - latitude);
        coordinates.put("maxLatitude", this.departure.getLatitude() + latitude);
        coordinates.put("minLongitude", calculateArrival(radius, 270).getLongitude());
        coordinates.put("maxLongitude", calculateArrival(radius, 90).getLongitude());
        return coordinates;
    }


    /**
     * Get a GlobalCoordinates object with the arrival coordinates from the given bearing and distance from the departure point.
     * The departure point is the one set in initGeodesy().
     * @param miles: The distance in miles.
     * @param direction: The direction in degrees.
     * @return A GlobalCoordinates object with the arrival coordinates, including the latitude and the longitude.
     */
    private GlobalCoordinates calculateArrival(double miles, double direction){
        double distance = convertMilesToKilometers(miles) * 1000;
        return calculator.calculateEndingGlobalCoordinates(this.reference, this.departure, direction, distance, new double[1]);
    }

    /**
     * Initializes the GeodeticCalculator, and the GlobalCoordinates object with the departure coordinates.
     * @param latitude: The latitude of the departure.
     * @param longitude: The longitude of the departure.
     */
    private void initGeodesy(double latitude, double longitude) {
        this.calculator = new GeodeticCalculator();
        this.reference = Ellipsoid.WGS84;
        this.departure = new GlobalCoordinates(latitude, longitude);
    }
}















