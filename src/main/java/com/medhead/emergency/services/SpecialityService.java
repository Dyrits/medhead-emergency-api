package com.medhead.emergency.services;

import com.medhead.emergency.entities.Speciality;
import com.medhead.emergency.repositories.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpecialityService {
    private SpecialityRepository repository;

    @Autowired
    public void setRepository(SpecialityRepository repository){
        this.repository = repository;
    }

    public Speciality find(long id){
        return repository.findById(id).orElse(null);
    }

    public Iterable<Speciality> findAll(){
        return repository.findAll();
    }

    public Iterable<Speciality> findAllBy(String group){
        return repository.findALlByGroupName(group);
    }

}
