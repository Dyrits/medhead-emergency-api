package com.medhead.emergency.services;

import com.medhead.emergency.entities.SpecialityGroup;
import com.medhead.emergency.repositories.SpecialityGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpecialityGroupService {
    private SpecialityGroupRepository repository;

    @Autowired
    public void setRepository(SpecialityGroupRepository repository){
        this.repository = repository;
    }

    public SpecialityGroup find(Long id){
        return repository.findById(id).orElse(null);
    }

    public Iterable<SpecialityGroup> findAll(){
        return repository.findAll();
    }
}
