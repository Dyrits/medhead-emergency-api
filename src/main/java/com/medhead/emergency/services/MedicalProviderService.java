package com.medhead.emergency.services;

import com.medhead.emergency.entities.MedicalProvider;
import com.medhead.emergency.repositories.MedicalProviderRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Data
@Service
@AllArgsConstructor
public class MedicalProviderService {

    private MedicalProviderRepository repository;

    @Autowired
    public void setRepository(MedicalProviderRepository repository){
        this.repository = repository;
    }

    public MedicalProvider find(long id){
        return repository.findById(id).orElse(null);
    }

    public Iterable<MedicalProvider> findAll(){
        return repository.findAll();
    }

    public Iterable<MedicalProvider> findAllBy(String speciality, int availability, Map<String, Double> coordinates){
        return repository.findAllBySpecialitiesNameAndAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
            speciality,
            availability,
            coordinates.get("minLatitude"),
            coordinates.get("maxLatitude"),
            coordinates.get("minLongitude"),
            coordinates.get("maxLongitude")
        );
    }

    public Iterable<MedicalProvider> findAllBy(int availability, Map<String, Double> coordinates){
        return repository.findAllByAvailabilityGreaterThanEqualAndLatitudeIsBetweenAndLongitudeIsBetween(
                availability,
                coordinates.get("minLatitude"),
                coordinates.get("maxLatitude"),
                coordinates.get("minLongitude"),
                coordinates.get("maxLongitude")
        );
    }

    public MedicalProvider save(MedicalProvider medicalProvider){
        return repository.save(medicalProvider);
    }

    public void delete(long id){
        repository.deleteById(id);
    }



}
