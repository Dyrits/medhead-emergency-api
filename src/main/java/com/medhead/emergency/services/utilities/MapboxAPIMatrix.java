package com.medhead.emergency.services.utilities;

import com.medhead.emergency.entities.MedicalProvider;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@UtilityClass
@Slf4j
public class MapboxAPIMatrix {
    private static final String key = "pk.eyJ1IjoiZHlyaXRzIiwiYSI6ImNsMXh2MmU0ZzA2aGYza28zamo1b281M2sifQ.qC8DzfpT1pGLMgDY8X6BvQ";
    private static final String url = "https://api.mapbox.com/directions-matrix/v1/mapbox/driving/";
    private static final String parameters = "?sources=0&annotations=duration&access_token=" + key;

    /**
     * Search for the closest medical provider using the Mapbox Matrix API.
     * @param departure: A departure location, in a GlobalCoordinates (Geodesy) object including the latitude and the longitude.
     * @param providers: An Iterable of MedicalProvider objects that will be used to calculate the distance between the departure and themselves.
     * @return The closest MedicalProvider to the given coordinates.
     * @throws ProtocolException: If the HTTP request is not valid.
     */
    public MedicalProvider search(GlobalCoordinates departure, Iterable<MedicalProvider> providers) throws ProtocolException {
        StringBuilder builder = new StringBuilder();
        builder.append(url).append(departure.getLatitude()).append(",").append(departure.getLatitude()).append(";");
        for (MedicalProvider provider : providers) {
            builder.append(provider.getLongitude()).append(",").append(provider.getLatitude()).append(";");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append(parameters);
        HttpURLConnection connection = null;
        try {
            log.info("Requesting: " + builder.toString());
            URL url = new URL(builder.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            InputStream stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            reader.close();
            JSONObject json = new JSONObject(response.toString());
            JSONArray array = json.getJSONArray("durations").getJSONArray(0);
            List<Double> durations = new ArrayList<>();
            for (int index = 1; index < array.length(); index++) { durations.add(array.getDouble(index)); }
            int index = durations.indexOf(Collections.min(durations));
            return IterableUtils.toList(providers).get(index);
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        } finally { if (connection != null) { connection.disconnect(); } }
    }

}
