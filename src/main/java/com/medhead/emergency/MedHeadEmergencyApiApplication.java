package com.medhead.emergency;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MedHeadEmergencyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedHeadEmergencyApiApplication.class, args);
    }

}
