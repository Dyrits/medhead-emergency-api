package com.medhead.emergency.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@DynamicUpdate
public class SpecialityGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long groupId;

    private String name;

    @JsonBackReference
    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Speciality> specialities = new ArrayList<>();

    public void addSpeciality(Speciality speciality) {
        specialities.add(speciality);
        speciality.setGroup(this);
    }

    public void removeSpeciality(Speciality speciality) {
        specialities.remove(speciality);
        speciality.setGroup(null);
    }
}
