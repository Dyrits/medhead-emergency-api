package com.medhead.emergency.controllers;

import com.medhead.emergency.entities.Speciality;
import com.medhead.emergency.services.SpecialityService;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpecialityController {

    private SpecialityService service;
    @Autowired
    public void setService(SpecialityService service) {
        this.service = service;
    }

    @GetMapping("/groups/specialities")
    public ResponseEntity<Iterable<Speciality>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/groups/{group}/specialities")
    public ResponseEntity<Iterable<Speciality>> findAllBy(@PathVariable("group") String group) {
        Iterable<Speciality> specialities = service.findAllBy(group);
        if (IterableUtils.size(specialities) == 0) { return ResponseEntity.notFound().build(); }
        return ResponseEntity.ok(specialities);
    }

}
