package com.medhead.emergency.controllers;

import com.medhead.emergency.entities.MedicalProvider;
import com.medhead.emergency.services.MedicalProviderSearcher;
import com.medhead.emergency.services.MedicalProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.ProtocolException;

@RestController
public class MedicalProviderController {

    private MedicalProviderService service;
    private MedicalProviderSearcher searcher;

    @Autowired
    public void setService(MedicalProviderService service) {
        this.service = service;
    }

    @Autowired
    public void setSearcher(MedicalProviderSearcher searcher) {
        this.searcher = searcher;
    }

    @GetMapping("/medical-providers")
    public ResponseEntity<Iterable<MedicalProvider>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/medical-providers/search")
    public ResponseEntity<MedicalProvider> search(
            @RequestParam(required = true) double latitude,
            @RequestParam(required = true) double longitude,
            @RequestParam(required = false) String speciality,
            @RequestParam(required = false, defaultValue = "1") int amount
    ) throws ProtocolException {
        /* The service dedicated to the reservation is not available yet.
        This route will send a reservation (with or without the NHS number) to the medical provider when the service will be available.
        It can be done using a message queue (such as RabbitMQ or Kafka).
        Note: A listener can be used to update the medical provider's availability.
        */
        MedicalProvider provider = searcher.search(speciality, amount, latitude, longitude);
        if (provider == null) { return ResponseEntity.notFound().build(); }
        // The amount of bed available is decreased by the amount required.
        provider.setAvailability(provider.getAvailability() - amount);
        return ResponseEntity.ok(service.save(provider));
    }
}
