package com.medhead.emergency.controllers;

import com.medhead.emergency.entities.SpecialityGroup;
import com.medhead.emergency.services.SpecialityGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpecialityGroupController {
    SpecialityGroupService service;

    @Autowired
    public void setService(SpecialityGroupService service) {
        this.service = service;
    }

    @GetMapping("/groups")
    public ResponseEntity<Iterable<SpecialityGroup>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }
}
