/*
 * Inserting all the NHS groups of specialities.
 */
INSERT INTO speciality_group (name)
VALUES ('Anaesthetics'),
       ('Clinical oncology'),
       ('Dental'),
       ('Emergency medicine'),
       ('General medecine'),
       ('Obstetrics & gynaecology'),
       ('Paediatric'),
       ('Pathology'),
       ('PHM & CHS'),
       ('Psychiatry'),
       ('Radiology'),
       ('Surgical');
/*
 * Inserting all the NHS specialities.
 */
INSERT INTO speciality (name, group_id)
VALUES ('Anaesthetics', '1'),
       ('Intensive care medicine', '1'),
       ('Clinical oncology', '2'),
       ('Additional dental specialties', '3'),
       ('Dental and Maxillofacial Radiology', '3'),
       ('Endodontics', '3'),
       ('Oral and Maxillofacial Pathology', '3'),
       ('Oral and maxillo-facial surgery', '3'),
       ('Oral Medicine', '3'),
       ('Oral Surgery', '3'),
       ('Orthodontics', '3'),
       ('Paediatric dentistry', '3'),
       ('Periodontics', '3'),
       ('Prosthodontics', '3'),
       ('Restorative dentistry', '3'),
       ('Special Care Dentistry', '3'),
       ('Emergency Medicine', 4),
       ('Acute Internal Medicine', '5'),
       ('Allergy', '5'),
       ('Audio Vestibular Medicine', '5'),
       ('Cardiology', '5'),
       ('Clinical genetics', '5'),
       ('Clinical neurophysiology', '5'),
       ('Clinical pharmacology and therapeutics', '5'),
       ('Dermatology', '5'),
       ('Endocrinology and diabetes mellitus', '5'),
       ('Gastroenterology', '5'),
       ('General (internal) medicine', '5'),
       ('General Med Practitioner', '5'),
       ('General Practice (GP) 6 month Training', '5'),
       ('Genito-urinary medicine', '5'),
       ('Geriatric medicine', '5'),
       ('Infectious diseases', '5'),
       ('Medical oncology', '5'),
       ('Medical ophthalmology', '5'),
       ('Neurology', '5'),
       ('Occupational medicine', '5'),
       ('Other', '5'),
       ('Palliative medicine', '5'),
       ('Rehabilitation medicine', '5'),
       ('Renal medicine', '5'),
       ('Respiratory medicine', '5'),
       ('Rheumatology', '5'),
       ('Sport and Exercise Medicine', '5'),
       ('Community Sexual and Reproductive Health', '6'),
       ('Obstetrics and Gynaecology', '6'),
       ('Paediatric cardiology', '7'),
       ('Paediatrics', '7'),
       ('Chemical pathology', '8'),
       ('Diagnostic Neuropathology', '8'),
       ('Forensic Histopathology', '8'),
       ('General pathology', '8'),
       ('Haematology', '8'),
       ('Histopathology', '8'),
       ('Immunology', '8'),
       ('Medical microbiology', '8'),
       ('Paediatric and Perinatal Pathology', '8'),
       ('Virology', '8'),
       ('Community Health Service Dental', '9'),
       ('Community Health Service Medical', '9'),
       ('Dental Public Health', '9'),
       ('General Dental Practitioner', '9'),
       ('Public health medicine', '9'),
       ('Child and adolescent psychiatry', '10'),
       ('Forensic psychiatry', '10'),
       ('General psychiatry', '10'),
       ('Old age psychiatry', '10'),
       ('Psychiatry of learning disability', '10'),
       ('Psychotherapy', '10'),
       ('Clinical radiology', '11'),
       ('Nuclear medicine', '11'),
       ('Cardio-thoracic surgery', '12'),
       ('General surgery', '12'),
       ('Neurosurgery', '12'),
       ('Ophthalmology', '12'),
       ('Otolaryngology', '12'),
       ('Paediatric surgery', '12'),
       ('Plastic surgery', '12'),
       ('Trauma and orthopaedic surgery', '12'),
       ('Urology', '12'),
       ('Vascular Surgery', '12');

/*
 * Inserting a sample of medical provides.
 */
INSERT INTO medical_provider (name, address, availability, latitude, longitude)
VALUES ('St. Thomas'' Hospital', 'Westminster Bridge Rd, London SE1 7EH, United Kingdom', 20, 51.498016, -0.118011),
       ('Vale Hospital', 'Hensol Castle Park, Hensol Rd, Pontyclun CF72 8JX, United Kingdom', 5, 51.504189, -3.376255),
       ('St. James'' Institute of Oncology/Leeds Cancer Centre',
        'Bexley Wing, Beckett St, Harehills, Leeds LS9 7LP, United Kingdom', 10, 53.805691, -1.523557),
       ('Nuffield Health Bristol Hospital', '3 Clifton Hill, Clifton, Bristol BS8 1BN, United Kingdom', 0, 51.453476,
        -2.615936),
       ('St. George''s Hospital', 'Blackshaw Rd, London SW17 0QT, United Kingdom', 25, 51.425148, -0.174911),
       ('Thurloe Street Dental and Implant Centre', '10 Thurloe St, South Kensington, London SW7 2ST, United Kingdom',
        20, 51.494431, -0.172890),
       ('Watford General Hospital', 'Vicarage Rd, Watford WD18 0HB, United Kingdom', 20, 51.649113, -0.404372);

/*
 * Randomly assigning medical providers to specialities.
 */
INSERT INTO providers_specialties (provider_id, speciality_id)
VALUES (1, 1), (1, 2), (1, 22), (1, 23), (1, 24), (1, 28), (1, 29), (1, 79), (1, 80), (1, 81),
       (2, 30), (2, 31), (2, 32), (2, 33), (2, 34), (2, 38), (2, 39), (2, 40), (2, 41), (2, 42),
       (3, 34), (3, 35), (3, 36), (3, 37), (3, 38), (3, 39), (3, 54), (3, 55), (3, 56),
       (4, 10), (4, 15), (4, 16), (4, 17), (4, 18), (4, 18), (4, 19), (4, 24), (4, 25), (4, 26),
       (5, 24), (5, 25), (5, 32), (5, 33), (5, 68), (5, 78), (5, 79), (5, 80), (5, 81),
       (6, 12), (6, 13), (6, 14), (6, 27), (6, 28), (6, 35), (6, 39), (6, 44), (6, 45), (6, 46), (6, 59), (6, 60), (6, 61), (6, 62), (6, 63),
       (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 35), (7, 36), (7, 37), (7, 64);















