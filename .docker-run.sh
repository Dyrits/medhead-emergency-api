#!/bin/bash
DEVELOPMENT=-Dspring-boot.run.profiles=development
# ./mvnw is an executable shell script used in place of a fully installed Maven.
./mvnw spring-boot:run $DEVELOPMENT

# Different goals can be used, for example:
# ./mvnw clean package $DEVELOPMENT
# java -jar /app/target/MedHead-Emergency-API-0.0.1-SNAPSHOT.jar